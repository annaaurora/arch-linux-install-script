#!/bin/sh

# Variables

## Root partition
RootPart="-o subvolid=447 /dev/sda8"
## Boot partition
BootPart="/dev/sda5"
## Swap partition
SwapPart="/dev/sda4"
## Home partition
HomePart="-o subvolid=256 /dev/sda8"

## Kernel
Kernel="linux-zen"

## Grub bootloader entry name
GrubName="GRUWU"

## Time zone
Region=""
City=""

## Keyboard layout
Keymap=""

## Hostname
Hostname="ArchLinuxBox"

## Dns servers
DnsServers="1.1.1.1 1.0.0.1"

## Btrfs root parition
BtrfsRoot="true"

# Cpu vendor
CPUVendor="AMD"

# Install

## Mounting partitions and creating mountpoints

mount $RootPart /mnt
mkdir /mnt/boot
mount $BootPart /mnt/boot
swapon $SwapPart
mkdir /mnt/home
mount $HomePart /mnt/home

## Install essential packages
pacstrap /mnt base $Kernel linux-firmware base-devel neovim neofetch dhcpcd grub efibootmgr

## Genereate /etc/fstab file
genfstab -U /mnt >> /mnt/etc/fstab

## Chroot
arch-chroot /mnt

## Btrfs packages

### If root parition is btrfs, install btrfs-progs
if [[ $BtrfsRoot=true ]]; then
  pacman -S grub-btrfs
fi

### If root parition is btrfs, install package for grub to be able to boot from snapshots
if [[ $BtrfsRoot=true ]]; then
  pacman -S grub-btrfs
fi

## Set time zone
ln -sf /usr/share/zoneinfo/$Region/$City /etc/localtime
hwclock --systohc

## Localization

### Uncomment needed locales
nvim /etc/locale.gen

### Generate locales
locale-gen

### Set locales
nvim /etc/locale.conf

## Set keyboard layout
echo "KEYMAP=$Keymap" >> /etc/vconsole.conf

## Networking

### Set hostname
echo "$Hostname" >> /etc/hostname

### Hosts
echo "127.0.0.1	localhost
::1		localhost" >> /etc/hosts

### Enable and start dhcpcd for networking
systemctl enable dhcpd && systemctl start dhcpcd

### Set dns server
echo "static domain_name_servers=$DnsServers" > /etc/dhcpcd.conf

## Initramfs
mkinitcpio -P

## Set root password
passwd

## Install ucode
if [[ $CPUVendor="AMD" ]]; then
  pacman -S amd-ucode
else
  pacman -S intel-ucode
fi

## GRUB

### Install
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=$GrubName

### Make configuration file
grub-mkconfig -o /boot/grub/grub.cfg
