# Arch Linux install script

## Description

A very simple Arch Linux install shell script without partitioning.

## Note

This install script only work on the x86_64 architecture as Arch Linux and only if your system supports UEFI.

This install script has not been tested. You shouldn't try this out yet.

## Instructions

1. Partition and format your your disk(s) to have 1 boot partition, 1 root partition, 1 swap parition and 1 home partition. I like to do this graphically with **GParted** for example. The Arch Linux wiki [suggests](https://wiki.archlinux.org/title/Installation_guide#Partition_the_disks) to make the EFI boot partition at least 260 MiB. The size of the swap partition depends on how much RAM you have and how much you need. The difference of the RAM you need minus the RAM you have is the size of you swap partition. Root and home partition make up the rest of your disk if there isn't anything else on it. If you like `btrfs`, a filesystem with snapshots to rollback and other nice features you can also have the root and home partition as subvolumes on one partition. For more Instructions on how to setup btrfs visit [Chris Titus' Btrfs Guide](https://christitus.com/btrfs-guide/).
2. Download the Arch Linux image through a torrent or through one of the `https` mirrors listed on the [Arch Linux downloads page](https://archlinux.org/download/)
3. Verify the validity of the image like described [here](https://wiki.archlinux.org/title/Installation_guide#Verify_signature).
4. [Prepare an installation medium](https://wiki.archlinux.org/title/Installation_guide#Prepare_an_installation_medium)
5. [Boot the live environment](https://wiki.archlinux.org/title/Installation_guide#Boot_the_live_environment)
6. Do

        wget https://gitlab.com/papojari/arch-linux-install-script/-/raw/master/archlinuxinstallscript.sh

   , edit the variables of the script with neovim for example

        nvim archlinuxinstallscript.sh

   and run the script with

        sh archlinuxinstallscript.sh

7. Reboot with

        reboot

## License

This project is licensed under the **GNU General Public License v3.0** also called **GPLv3**. Read the full license at `LICENSE`.
